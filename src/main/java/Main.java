import sun.nio.ch.Net;

import java.io.IOException;
import java.net.ServerSocket;

public class Main implements EndListener {

    public void onEnd(ProductAutomaton productAutomaton, Network_auto network_auto){
        if(!network_auto.checkForEnd(productAutomaton)){
            return;
        } else {
            productAutomaton.print();
            productAutomaton.printStatistics();
        }
    }

    public static void main(String[] args) throws IOException {

        Network_auto network = new Network_auto();

        // automaton 0
        Automaton automaton = new Automaton(1, 4);
        automaton.addTransition(1, 2, 1);
        automaton.addTransition(2, 3, 2);
        automaton.addTransition(3, 4, 3);
        automaton.setCurrent(1);
        automaton.setNodeInitial(1);
        automaton.setNodeFinal(4);
        network.addAutomaton(automaton);

        // automaton 1
        automaton = new Automaton(5, 8);
        automaton.addTransition(5, 6, 4);
        automaton.addTransition(6, 7, 5);
        automaton.addTransition(7, 8, 6);
        automaton.setCurrent(5);
        automaton.setNodeInitial(5);
        automaton.setNodeFinal(8);
        network.addAutomaton(automaton);

        // automaton 2
        automaton = new Automaton(9, 12);
        automaton.addTransition(9, 10, 7);
        automaton.addTransition(10, 11, 8);
        automaton.addTransition(11, 12, 9);
        automaton.setCurrent(9);
        automaton.setNodeInitial(9);
        automaton.setNodeFinal(12);
        network.addAutomaton(automaton);

        // automaton 3
        automaton = new Automaton(13, 14);
        automaton.addTransition(13, 14, 1);
        automaton.addTransition(13, 14, 4);
        automaton.addTransition(14, 14, 4);
        automaton.setCurrent(13);
        automaton.setNodeInitial(13);
        automaton.setNodeFinal(14);
        network.addAutomaton(automaton);

        // automaton 4
        automaton = new Automaton(15, 16);
        automaton.addTransition(15, 16, 1);
        automaton.addTransition(16, 16, 2);
        automaton.addTransition(16, 16, 5);
        automaton.setCurrent(15);
        automaton.setNodeInitial(15);
        automaton.setNodeFinal(16);
        network.addAutomaton(automaton);

        // automaton 5
        automaton = new Automaton(17, 18);
        automaton.addTransition(17, 18, 3);
        automaton.addTransition(17, 18, 5);
        automaton.addTransition(18, 18, 5);
        automaton.addTransition(18, 18, 6);
        automaton.addTransition(18, 18, 9);
        automaton.setCurrent(17);
        automaton.setNodeInitial(17);
        automaton.setNodeFinal(18);
        network.addAutomaton(automaton);

        // automaton 6
        automaton = new Automaton(19, 20);
        automaton.addTransition(19, 20, 2);
        automaton.addTransition(20, 20, 4);
        automaton.addTransition(20, 20, 5);
        automaton.setCurrent(19);
        automaton.setNodeInitial(19);
        automaton.setNodeFinal(20);
        network.addAutomaton(automaton);

        // automaton 7
        automaton = new Automaton(21, 22);
        automaton.addTransition(21, 22, 5);
        automaton.addTransition(22, 22, 7);
        automaton.addTransition(22, 22, 8);
        automaton.setCurrent(21);
        automaton.setNodeInitial(21);
        automaton.setNodeFinal(22);
        network.addAutomaton(automaton);

        // automaton 8
        automaton = new Automaton(23, 24);
        automaton.addTransition(23, 24, 4);
        automaton.addTransition(23, 24, 7);
        automaton.addTransition(24, 24, 3);
        automaton.addTransition(24, 24, 5);
        automaton.addTransition(24, 24, 7);
        automaton.setCurrent(23);
        automaton.setNodeInitial(23);
        automaton.setNodeFinal(24);
        network.addAutomaton(automaton);

        network.countTransitions();

        //ServerSocket socket = new ServerSocket(1337);
        //SocketHandler socketHandler = new SocketHandler(socket.accept(),0, network);
        ProductAutomaton productAutomaton = ProductAutomaton.getInstance(network);
        //int clientNumber = 0;

        /*try(ServerSocket socket = new ServerSocket(1337);){
            while(true){
                new SocketHandler(socket.accept(),clientNumber++, network).start();
            }
        }*/
        //new SolveThread(network, productAutomaton).start();
        productAutomaton=network.initState(productAutomaton);
        //productAutomaton=network.solveWglab(productAutomaton);
        new SolveThread(network, productAutomaton).start();
        new SocketMaker(network, productAutomaton).start();


        while(!network.checkForEnd(productAutomaton)){}

        /*try(ServerSocket socket = new ServerSocket(1337);){
            while(!network.checkForEnd(productAutomaton)){
                new SocketHandler(socket.accept(),clientNumber++, network, productAutomaton).start();
            }
        }*/


        /*try {
            socketHandler.receiveConnection();
            socketHandler.sendData(network);
        } catch (Exception e){
            e.printStackTrace();
        }

        ProductAutomaton productAutomaton = socketHandler.recieveData();*/

        productAutomaton.print();
        //productAutomaton.printStatistics();
        //ProductAutomaton productAutomaton = network.solve();
    }
}
